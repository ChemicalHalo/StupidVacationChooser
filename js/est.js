
//Variables

var pages = [
    new Page("Choose a LandScape That Catches Your Eyes", "Pick a Landscape", [
        new Question("Question 1", "img/hawaii-landscape.jpg",0),
        new Question("Question 2", "img/bb-landscape.jpg", 1),
        new Question("Question 3", "img/japan-landscape.jpg", 2),
        new Question("Question 4", "img/iceland-landscape.jpg", 3),
        new Question("Question 5", "img/paris-landscape.jpg", 4),
        new Question("Question 6", "img/budapest-landscape.jpg", 5)
    ]),
    new Page("Choose a Type of Dish That You Would Enjoy", "Pick a Meal", [
        new Question("Question 7", "img/hawaii-food.jpg", 0),
        new Question("Question 8", "img/bb-food.jpg", 1),
        new Question("Question 9", "img/japan-food.jpg", 2),
        new Question("Question 10", "img/iceland-food.jpg", 3),
        new Question("Question 11", "img/paris-food.jpg", 4),
        new Question("Question 12", "img/budapest-food.jpg", 5)
    ]),
    new Page("Choose a Type of Architecture that you Find Attractive", "Pick a Place", [
        new Question("Question 13", "img/hawaii-building.jpg", 0),
        new Question("Question 14", "img/bb-building.jpg", 1),
        new Question("Question 15", "img/japan-building.jpg", 2),
        new Question("Question 16", "img/iceland-building.jpg", 3),
        new Question("Question 17", "img/paris-building.jpg", 4),
        new Question("Question 18", "img/budapest-building.jpg", 5)
    ]),
    new Page("Choose Which View Point You Find More Attractive", "Pick a View", [
        new Question("Question 19", "img/hawaii-VP.jpg", 0),
        new Question("Question 20", "img/iceland-VP.jpg", 1),
        new Question("Question 21", "img/japan-VP.jpg", 2),
        new Question("Question 22", "img/bb-VP.jpg", 3),
        new Question("Question 23", "img/paris-VP.jpg", 4),
        new Question("Question 24", "img/budapest-VP.jpg", 5)
    ]),
    new Page("Choose Which Sky You Find More Attractive", "Pick Your Sky", [
        new Question("Question 25", "img/hawaii-sky.jpg", 0),
        new Question("Question 26", "img/iceland-sky.jpg", 1),
        new Question("Question 27", "img/japan-sky.jpg", 2),
        new Question("Question 28", "img/bb-sky.jpg", 3),
        new Question("Question 29", "img/paris-sky.jpg", 4),
        new Question("Question 30", "img/budapest-sky.jpg", 5)
    ])
];

var categories=[
	new Place("Hawaii", "<a href='http://www.gohawaii.com/en/big-island/'>Hawaii Welcomes You!</a>", 'img/hawaii-VP.jpg'),
	new Place("Iceland", "<a href=''https://iceland.nordicvisitor.com/?rf=g01-63&gclid=CjwKEAjwubK4BRC1xczKrZyj3mkSJAC6ntgrhX27-BLiFJVUcSdnEccbH5iyu5h7mtgkKlApwNUoHxoCllTw_wcB'>Visit Beautiful Iceland</a>", 'img/iceland-building.jpg'),
	new Place("Japan", "<a href='http://www.visitjapan.jp/en/'>Go to Japan, Enjoy the cherry blossoms!</a>", 'img/japan-landscape.jpg'),
	new Place("Bora Bora", "<a href='http://www.tahiti.com/island/bora-bora'>Relax on Bora Bora</a>", 'img/bb-VP.jpg'),
	new Place("Paris", "<a href='http://en.parisinfo.com/'>Bon! Paris is your destination.</a>", 'img/paris-VP.jpg'),
	new Place("Budapest", "<a href='http://visitbudapest.travel/'>The beauty of Budapest awaits you.</a>", 'img/budapest-sky.jpg')
];


//set up page

var answers = document.querySelectorAll("li[id^=\"option\"]");

var currentPage;

var artHeader;

var artSubHead;

reset();


//Functions and handlers

//score changer
function ansClick(){
    for(i in pages[currentPage].questions){
        if(this.innerHTML == pages[currentPage].questions[i].text){
	        categories[pages[currentPage].questions[i].category].total+=
	        	1*(pages[currentPage].questions[i].weight)
        }
    }
    nextPage();
}

//go to next page
function nextPage(){
    currentPage++;

    if(currentPage<pages.length){
        artHeader = document.querySelector("#container #content article>header>h3").innerHTML = pages[currentPage].header;

		artSubHead = document.querySelector("#container #content article>header>h4").innerHTML = artSubHead = pages[currentPage].subhead;

		ansSetup();

    } else {
        artHeader = document.querySelector("#container #content article>header>h3").innerHTML =
        	"Summary";

        subhead = document.querySelector("#container #content article>header>h4").innerHTML =
        	"Final breakdown of vacation location";

    	document.querySelector("article ul").className="hide";

		//var chk = categories[0].total;
		var lrg = Math.max.apply(Math, categories.map(function(o){return o.total}));
		var lrgE =0;
		var objChk =[];
		for(i in categories){
			if(categories[i].total === lrg){
				if(lrgE>=2){
					objChk.push(i);
				} else {
					objChk[0] = i;
				}
				lrgE++;
			}
		}

    	var docOut ="";

    	if(objChk.length>1){
	    	docOut += "<p>You chose multiple places!</p>";
    	} else {
	    	docOut += "<p><img src='"+categories[objChk[0]].imageURL+"'><h4>"+categories[objChk[0]].named+"</h4></p>";
	    	docOut +="<p>"+categories[objChk[0]].desc+"</p><br />"
    	}
		docOut += "<ul>";

    	for(i in categories){
	    	if(categories[i].total > 0){
		    	docOut+=
				"<li>"+categories[i].named+": "+
	    		categories[i].total*categories[i].weight+
	    		" points.</li>\n" + "";
	    	}
	    }
    	docOut+="</ul>\n<button id=\"reset\">Try Again</button";

    	document.getElementById("output").innerHTML=docOut;

    	document.getElementById("reset").addEventListener('click', reset);
    }


}

//reset to main
function reset(){
    shuffle(pages);

	currentPage = 0;

	artHeader = document.querySelector("#container #content article>header>h3").innerHTML = pages[currentPage].header;

	artSubHead = document.querySelector("#container #content article>header>h4").innerHTML = artSubHead = pages[currentPage].subhead;

	document.getElementById("output").innerHTML="";

	document.querySelector("article ul").className="";

	for(i in categories){
    	categories[i].total = 0;
	}

	//shuffle questions
	for(i in pages){
    	shuffle(pages[i].questions);
	}

    ansSetup();


}

//answer setup
function ansSetup(){
    for(var i=0;i<answers.length;i++){
        document.getElementById(answers[i].id).addEventListener('click', ansClick);
	    document.getElementById(answers[i].id).innerHTML=pages[currentPage].questions[i].text;
	    document.getElementById(answers[i].id).style.background = "url("+ pages[currentPage].questions[i].imageURL+") left top";
    }
    for(var i=0;i<pages.length;i++){
        for(var j=0;j<pages[i].length;i++){
			shuffle(pages[j].questions);
    	}
    }

}

//shuffle an array
function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

//class Constructors
function Place(named, desc, imgURL){
    this.total= 0;
    this.named= named;
    this.desc= desc;
    this.weight=1;
    this.imageURL=imgURL;
}

function Page(header, subhead, questions){
    this.header= header;
    this.subhead = subhead;
    this.questions = [];
    this.questions = questions;
}

function Question(text, imageURL, category){
    this.text=text;
    this.imageURL = imageURL;
    this.category = category;
    this.weight=1;
}
